-- IV
-- 1
SELECT maker FROM Product
WHERE maker 
    NOT IN (SELECT maker 
        FROM Product 
        WHERE type IN ('Laptop'))
            AND type IN ('PC')
GROUP BY maker
ORDER BY maker;
-- 2
SELECT maker FROM Product
WHERE maker 
    <> ALL (SELECT maker 
            FROM Product 
            WHERE type IN ('Laptop'))
                AND type IN ('PC')
GROUP BY maker
ORDER BY maker;
-- 3
SELECT maker FROM Product
WHERE maker 
    = ANY (SELECT maker FROM product WHERE maker NOT IN -- all makers who doesn't produce laptop
           (SELECT maker  -- all makers who produce laptops
            FROM Product 
            WHERE type IN ('Laptop')))
		AND type IN ('PC')
GROUP BY maker
ORDER BY maker;
-- 4
/*SELECT maker FROM Product
WHERE maker IN ((SELECT DISTINCT maker m1 FROM Product WHERE type = 'PC') -- those who produce PC and Laptops
                
                (SELECT DISTINCT maker m2 FROM Product WHERE type = 'Laptop') )
GROUP BY maker
ORDER BY maker; */
-- V
-- 1
select distinct maker from product -- not my
where type = 'PC' AND maker not in (
select  maker  from product
where type = 'PC' AND (model <> ALL (select distinct model from pc)) );
/*
SELECT maker FROM Product -- wrong ((
WHERE NOT EXISTS (SELECT * FROM PC);
*/
-- 2
SELECT DISTINCT maker FROM Product RIGHT JOIN PC ON Product.model = PC.model
WHERE speed >= 750;
-- 3

SELECT DISTINCT Pr.maker FROM PC Pc LEFT JOIN Product Pr ON Pc.model = Pr.model
WHERE speed >= 750 AND Pr.maker IN
			(SELECT DISTINCT maker FROM Product
			WHERE maker in 
				(SELECT Pr.maker FROM Laptop L LEFT JOIN Product Pr ON L.model = Pr.model
				WHERE speed >= 750 ));
       
-- 4 

SELECT DISTINCT maker FROM PC LEFT JOIN Product Pro ON Pro.model = PC.model
WHERE speed = (SELECT MAX(speed) FROM PC) AND maker in
(SELECT DISTINCT maker FROM Printer Pri LEFT JOIN Product Pro ON Pro.model = Pri.model);

-- 5

SELECT name, launched, displacement FROM Ships S LEFT JOIN Classes C ON S.class = C.class
WHERE C.displacement > 35000 AND S.launched >= 1922;

-- 6 
SELECT class FROM Outcomes O LEFT JOIN Ships S ON O.ship = S.name; -- bad select because very strange DATABASE ships

-- VI
-- 1
SET @avgprice = (SELECT DISTINCT AVG(price) FROM Laptop);
SELECT DISTINCT CONCAT('середня ціна= ', @avgprice) AS middle_price FROM Laptop;


