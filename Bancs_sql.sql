DROP DATABASE Bancs_sql;
CREATE DATABASE IF NOT EXISTS Bancs_sql;
USE Bancs_sql;
CREATE TABLE IF NOT EXISTS Bancs (
    banc_id INT PRIMARY KEY,
    name VARCHAR(50),
    balance DECIMAL NOT NULL,
    reg_number INT NOT NULL,
    client_quantity INT,
    depo_quantity INT,
    credit_quantity INT
);
CREATE TABLE IF NOT EXISTS Clients_privat (
    id_number int PRIMARY KEY,
    name varchar(20) NOT NULL,
    surname varchar(20) NOT NULL,
    birth_year int NOT NULL,
    number_of_depo int,
    number_of_credit int,
    balance decimal NOT NULL
);
CREATE TABLE IF NOT EXISTS Tax_administration (
    id_number int PRIMARY KEY,
    name varchar(20) NOT NULL,
    surname varchar(20) NOT NULL,
    birth_year int NOT NULL,
    registration_date datetime NOT NULL,
    info varchar(50)
);
CREATE TABLE IF NOT EXISTS Accounts (
    account_id int PRIMARY KEY AUTO_INCREMENT,
    type varchar(10) NOT NULL,
    id_number int NOT NULL,
    balance decimal NOT NULL,
    rate int NOT NULL,
    currency_type varchar(10) NOT NULL
);
CREATE TABLE IF NOT EXISTS Transactions (
    transaction_id bigint PRIMARY KEY,
    id_number_from int NOT NULL,
    id_number_to int NOT NULL,
    suma decimal NOT NULL,
    date datetime NOT NULL
);
CREATE TABLE IF NOT EXISTS Currencies (
id_currency int PRIMARY KEY,
name varchar(20) NOT NULL,
kurs_to_usd decimal NOT NULL
);

ALTER TABLE Clients_privat ADD
    CONSTRAINT FK_Cients_Tax FOREIGN KEY
    (
    id_number
    ) REFERENCES Tax_administration (
    id_number
    
    ) ON DELETE CASCADE ON UPDATE RESTRICT ; -- just random strategy
 ALTER TABLE  Accounts ADD
    CONSTRAINT FK_Accounts_Cients FOREIGN KEY
    (
    id_number
    ) REFERENCES Clients_privat (
    id_number
    )ON DELETE RESTRICT ON UPDATE RESTRICT; -- just random strategy

ALTER TABLE Transactions ADD
    CONSTRAINT FK_Transactions_Cients_from FOREIGN KEY
    (
    id_number_from
    ) REFERENCES Clients_privat (
    id_number
    )ON DELETE RESTRICT ON UPDATE RESTRICT; -- just random strategy

ALTER TABLE Transactions ADD
    CONSTRAINT FK_Transactions_Cients_to FOREIGN KEY
    (
    id_number_to
    ) REFERENCES Clients_privat (
    id_number
    );

INSERT INTO Bancs VALUES (123456789, 'Privat Banc', 1000, 123, 5, 2, 2);
--
INSERT INTO Tax_administration VALUES (1, 'Sirko', 'Opanas', 1976, NOW(), 'like lobsters');
INSERT INTO Tax_administration VALUES (2, 'John', 'Watson', 1936, NOW(), 'tall');
INSERT INTO Tax_administration VALUES (3, 'Ivan', 'Boxman', 1996, NOW(), 'funny');
INSERT INTO Tax_administration VALUES (4, 'Bob', 'Pintur', 1982, NOW(), 'boxxer');
INSERT INTO Tax_administration VALUES (5, 'Joseph', 'Petrenko', 2001, NOW(), 'angry');
INSERT INTO Tax_administration VALUES (6, 'Nero', 'Wolf', 1876, NOW(), 'sad');
--
INSERT INTO Clients_privat VALUES (1, 'Sirko', 'Opanas', 1976, 1, 0, 2000);
INSERT INTO Clients_privat VALUES (2, 'John', 'Watson', 1936, 2, 1, 2400);
INSERT INTO Clients_privat VALUES (3, 'Ivan', 'Boxman', 1996, 1, 2, 400);
INSERT INTO Clients_privat VALUES (4, 'Bob', 'Pintur', 1982, 0, 1, -4000);
INSERT INTO Clients_privat VALUES (5, 'Joseph', 'Petrenko', 2001, 1, 1, 40);
--
INSERT INTO Accounts VALUES(1, 'depo', 1, 2000, 0.12, 'usd');
INSERT INTO Accounts VALUES(2, 'depo', 2, 2000, 0.12, 'usd');
INSERT INTO Accounts VALUES(3, 'depo', 2, 1000, 0.12, 'usd');
INSERT INTO Accounts VALUES(default, 'credit', 2, 600, 0.15, 'usd');
INSERT INTO Accounts VALUES(default, 'depo', 3, 1600, 0.12, 'usd');
INSERT INTO Accounts VALUES(default, 'credit', 3, 600, 0.15, 'usd');
INSERT INTO Accounts VALUES(default, 'credit', 3, 600, 0.15, 'usd');
INSERT INTO Accounts VALUES(default, 'credit', 4, -4000, 0.15, 'usd');
INSERT INTO Accounts VALUES(default, 'depo', 5, 1600, 0.12, 'usd');
INSERT INTO Accounts VALUES(default, 'credit', 5, 1200, 0.15, 'usd');
--
INSERT INTO transactions VALUES (1,1,2,300,NOW());
INSERT INTO transactions VALUES (2,3,2,400,NOW());
INSERT INTO transactions VALUES (3,2,4,500,NOW());
INSERT INTO transactions VALUES (4,2,2,100,NOW());
INSERT INTO transactions VALUES (5,5,1,700,NOW());
--
INSERT INTO currencies VALUES (1, 'usd', 1);


